module.addToEvent("Command", "test", null, null, function(info) {
	irc.send(info.channel, "Hello!!!!!!!!!!!!!!!!!!!!!!"); // send a message to the channe
	print(info.channel.toString()); // show channel's custom toString() output (below)
});

// on channel #test.echelon, quakenet.org

// org.echelon.saucebot.irc.Channel #test.echelon: 3 users, topic: loeloeoe
// Users: 
// 	Javaucebot	~Javaucebo@hostname.org
// 	Q	(No IP tracked) (OPPED)
// 	MuffinTastic	~MuffinTas@MuffinTastic32.users.quakenet.org (OPPED)

module.addToEvent("Command", "test2", null, null, function(info) {
	print(info.channel.isOpped(irc.getUserByName("Q"))); // prints if Q is opped in the channel you ran this cmd in
});

module.addToEvent("Command", "reload", null, null, function(info) {
	moduleSystem.reload(); // reloads modules (memory saving, but doesn't remove or add modules based on config changes. moduleSystem.load() does that)
});