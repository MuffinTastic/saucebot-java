package org.echelon.saucebot.modules;

import org.echelon.saucebot.Config;
import org.echelon.saucebot.irc.IRC;
import org.echelon.saucebot.irc.MessageInfo;

import java.io.File;
import java.util.ArrayList;
import javax.script.ScriptEngineManager;

public class ModuleSystem {
	
	public enum EventType {
		COMMAND, MESSAGE, JOIN, LEAVE, KICK, QUIT, NICK, MODE, // IRC
		
		STARTUP, SHUTDOWN // Bot
	}
	
	private String folderName;
	private IRC ircInstance;
	private Config config;
	
	private ScriptEngineManager manager;
	
	private ArrayList<Module> modules;
	
	public ModuleSystem(String folderName, Config config) {
		
		this.folderName = folderName;
		
		this.config = config;
		
		this.modules = new ArrayList<Module>();
	}
	
	public void setup(IRC ircInstance) {
		this.ircInstance = ircInstance;
		this.manager = new ScriptEngineManager();
		this.manager.put("irc", this.ircInstance);
		this.manager.put("config", this.config);
		this.manager.put("moduleSystem", this);
	}

	public void load() {
		
		if (!this.modules.isEmpty())
			this.modules.clear();
		
		ArrayList<String> configModuleList = this.config.getModules();
		String[] files = configModuleList.toArray(new String[configModuleList.size()]);
		
		for (String moduleName : files) {
			String[] jsFilesToLoad = new String[files.length];
			
			int count = 0;
			for (String fileName : new File(this.folderName).list()) {
				if (fileName.endsWith(".js")) {
					if (moduleName.equals(fileName)) {
						jsFilesToLoad[count++] = fileName;
					}
				}
			}
			
			for (String jsFile : jsFilesToLoad) {
				Module module = new Module(this.folderName + "/" + jsFile, jsFile.split("\\.")[0], this.manager);
				module.load();
				this.modules.add(module);
			}
		}
		
	}
	
	public void reload() {
		
		for (Module module : this.modules) {
			module.load();
		}
		
	}
	
	public void callEvent(MessageInfo info, EventType eventType) {
		for (int i = 0; i < this.modules.size(); i++)
			this.modules.get(i).callEvent(info, eventType);
	}

	public String getFolderName() {
		return this.folderName;
	}

	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}

	public ArrayList<Module> getModules() {
		return this.modules;
	}
	
}