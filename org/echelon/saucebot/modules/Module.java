package org.echelon.saucebot.modules;

import org.echelon.saucebot.Saucebot;
import org.echelon.saucebot.irc.MessageInfo;
import org.echelon.saucebot.irc.Utils;

import jdk.nashorn.api.scripting.ScriptObjectMirror;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class Module {
	
	private String fileName;
	private String name;
	private String description;
	
	private ScriptEngineManager manager;
	
	private ScriptEngine engine;
	private Bindings bindings;
	
	private HashMap<ModuleSystem.EventType, ArrayList<EventCallback>> events;
	
	public Module(String fileName, String name, ScriptEngineManager manager) {
		this.fileName = fileName;
		this.name = name;
		this.description = "(No description)";
		
		this.manager = manager;
		this.events = Module.setupEvents();
	}
	
	public boolean load() {
		
		Module.clearEvents(this.events);
		
		this.engine = this.manager.getEngineByName("js");
		this.bindings = this.engine.getBindings(ScriptContext.ENGINE_SCOPE);
		
		this.bindings.put("module", this);
		
		try {
			this.engine.eval(Utils.readFile(this.fileName), this.bindings);
		} catch (ScriptException e) {
			System.out.printf("[MODULE %s] [LOAD ERROR] %s\n", this.name, e.getMessage());
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public void addToEvent(String event, String name, String desc, String usage, ScriptObjectMirror jsFunction) {
		if (!this.events.containsKey(Module.stringToEventType(event))) {
			System.out.printf("[MODULE %s] [LOAD ERROR] Event '%s' does not exist. Cannot add function to event, aborting..\n", this.name, event);
			return;
		} else {
			this.events.get(Module.stringToEventType(event)).add(new EventCallback(name, desc, usage, jsFunction));
		}
	}
	
	public void callEvent(MessageInfo info, ModuleSystem.EventType eventType) {
		
		ArrayList<EventCallback> eventCallbacks = this.events.get(eventType);
		try {
			if (eventType == ModuleSystem.EventType.COMMAND) {
				EventCallback callback = EventCallback.getByName(info.arguments[0].substring(Saucebot.getBot().getConfig().getPrefix().length()), eventCallbacks);
				callback.call(info);
			} else {
				for (EventCallback callback : eventCallbacks) {
					if (eventType == ModuleSystem.EventType.STARTUP || eventType == ModuleSystem.EventType.SHUTDOWN)
						callback.call();
					else
						callback.call(info);
				}
			}
		} catch (ScriptException e) {
			System.out.printf("[MODULE %s] [RUNTIME ERROR] %s\n", this.name, e.getMessage());
		}
	}
	
	private static ModuleSystem.EventType stringToEventType(String event) {
		if (event == "Command")
			return ModuleSystem.EventType.COMMAND;
		else if (event == "Message")
			return ModuleSystem.EventType.MESSAGE;
		else if (event == "Join")
			return ModuleSystem.EventType.JOIN;
		else if (event == "Leave")
			return ModuleSystem.EventType.LEAVE;
		else if (event == "Kick")
			return ModuleSystem.EventType.KICK;
		else if (event == "Quit")
			return ModuleSystem.EventType.QUIT;
		else if (event == "Nick")
			return ModuleSystem.EventType.NICK;
		else if (event == "Mode")
			return ModuleSystem.EventType.MODE;
		
		else if (event == "Startup")
			return ModuleSystem.EventType.STARTUP;
		else if (event == "Shutdown") 
			return ModuleSystem.EventType.SHUTDOWN;
		
		return null;
	}
	
//	private static String eventTypeToString(ModuleSystem.EventType eventType) {
//		
//		if (eventType == ModuleSystem.EventType.COMMAND) {
//			return "command";
//		} else if (eventType == ModuleSystem.EventType.MESSAGE) {
//			return "message";
//		} else if (eventType == ModuleSystem.EventType.JOIN) {
//			return "join";
//		} else if (eventType == ModuleSystem.EventType.LEAVE) {
//			return "leave";
//		} else if (eventType == ModuleSystem.EventType.KICK) {
//			return "kick";
//		} else if (eventType == ModuleSystem.EventType.QUIT) {
//			return "quit";
//		} else if (eventType == ModuleSystem.EventType.NICK) {
//			return "nick";
//		} else if (eventType == ModuleSystem.EventType.MODE) {
//			return "mode";
//		}
//		
//		else if (eventType == ModuleSystem.EventType.STARTUP) {
//			return "startup";
//		} else if (eventType == ModuleSystem.EventType.SHUTDOWN) {
//			return "shutdown";
//		}
//		
//		return null;
//	}
	
	private static HashMap<ModuleSystem.EventType, ArrayList<EventCallback>> setupEvents() {
		
		HashMap<ModuleSystem.EventType, ArrayList<EventCallback>> events = new HashMap<ModuleSystem.EventType, ArrayList<EventCallback>>();
		
		events.put(ModuleSystem.EventType.COMMAND, new ArrayList<EventCallback>());
		events.put(ModuleSystem.EventType.MESSAGE, new ArrayList<EventCallback>());
		events.put(ModuleSystem.EventType.JOIN, new ArrayList<EventCallback>());
		events.put(ModuleSystem.EventType.LEAVE, new ArrayList<EventCallback>());
		events.put(ModuleSystem.EventType.KICK, new ArrayList<EventCallback>());
		events.put(ModuleSystem.EventType.QUIT, new ArrayList<EventCallback>());
		events.put(ModuleSystem.EventType.NICK, new ArrayList<EventCallback>());
		events.put(ModuleSystem.EventType.MODE, new ArrayList<EventCallback>());

		events.put(ModuleSystem.EventType.STARTUP, new ArrayList<EventCallback>());
		events.put(ModuleSystem.EventType.SHUTDOWN, new ArrayList<EventCallback>());
		
		return events;
		
	}
	
	private static void clearEvents(HashMap<ModuleSystem.EventType, ArrayList<EventCallback>> events) {
		for (Map.Entry<ModuleSystem.EventType, ArrayList<EventCallback>> entry : events.entrySet()) {
			ArrayList<EventCallback> callbacks = entry.getValue();
			if (!callbacks.isEmpty())
				callbacks.clear();
		}
	}

	public String getFileName() {
		return this.fileName;
	}

	public String getName() {
		return this.name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
