package org.echelon.saucebot.modules;

import java.util.ArrayList;

import javax.script.ScriptException;

import org.echelon.saucebot.irc.MessageInfo;

import jdk.nashorn.api.scripting.ScriptObjectMirror;

public class EventCallback {
	private ScriptObjectMirror jsMirror;
	
	private String name;
	private String description;
	private String usage;
	
	public EventCallback(String name, String description, String usage, ScriptObjectMirror jsMirror) {
		this.name = (name != null) ? name : "(No name)";
		this.description = (description != null) ? description : "(No description)";
		this.usage = (usage != null) ? usage : "(No usage)";
		
		this.jsMirror = jsMirror;
	}
	
	public void call(MessageInfo info) throws ScriptException {
		this.jsMirror.call(null, info);
	}
	
	public void call() {
		this.jsMirror.call(null);
	}

	public String getName() {
		return this.name;
	}

	public String getDescription() {
		return this.description;
	}

	public String getUsage() {
		return this.usage;
	}
	
	public static EventCallback getByName(String name, ArrayList<EventCallback> callbacks) {
		for (EventCallback callback : callbacks) {
			if (callback.getName().equals(name)) {
				return callback;
			}
		}
		return null;
	}
}
