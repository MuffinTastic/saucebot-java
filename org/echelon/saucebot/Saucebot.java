package org.echelon.saucebot;

import org.echelon.saucebot.irc.IRC;
import org.echelon.saucebot.modules.ModuleSystem;

public class Saucebot {
	
	public static Saucebot botInstance;
	private Config config;
	private ModuleSystem moduleSystem;
	
	private IRC ircInstance;

	public static void main(String[] args) {
		
		botInstance = new Saucebot();
		
		botInstance.run();
		
	}
	
	private void run() {

		Runtime.getRuntime().addShutdownHook(new Thread()
		{
		    @Override
		    public void run()
		    {
		        Saucebot.getBot().stop();
		    }
		});

		this.config = new Config("./config.cfg");
		this.moduleSystem = new ModuleSystem("./modules", this.config);
		this.ircInstance = new IRC(this.config.getServer(), this.config.getPort(), this.config);
		this.moduleSystem.setup(ircInstance);
		this.moduleSystem.load();
		
		String line = null;
		
		while ((line = this.ircInstance.update()) != null)
			this.ircInstance.parse(line);
			
		this.stop();
	}
	
	public void stop() {
		this.ircInstance.quit("Shutting down...");
		this.ircInstance.cleanUp();
	}

	public IRC getIRC() {
		return this.ircInstance;
	}

	public Config getConfig() {
		return this.config;
	}

	public ModuleSystem getModuleSystem() {
		return this.moduleSystem;
	}

	public static Saucebot getBot() {
		return Saucebot.botInstance;
	}

}
