package org.echelon.saucebot;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.echelon.saucebot.irc.Channel;

import com.eclipsesource.json.*;
import com.eclipsesource.json.JsonObject.Member;

public class Config {

	private JsonObject jsonFile;
	private String fileName;
	
	private String server;
	private int port;
	
	private String nickname;
	private String description;
	private String prefix;
	
	private ArrayList<Channel> channels;
	private ArrayList<String> modules;
	
	public Config(String fileName) {
		this.fileName = fileName;
		this.reload();
	}
	
	public void reload() {
		try {
			FileReader fileReader = new FileReader(new File(this.fileName));
			this.jsonFile = Json.parse(fileReader).asObject();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
		
		this.server = this.jsonFile.get("server").asString();
		this.port = this.jsonFile.get("port").asInt();
		
		this.nickname = this.jsonFile.get("nickname").asString();
		this.description = this.jsonFile.get("description").asString();
		this.prefix = this.jsonFile.get("prefix").asString();
		
		JsonObject channels = this.jsonFile.get("channels").asObject();
		this.channels = new ArrayList<Channel>();
		
		JsonArray modules = this.jsonFile.get("modules").asArray();
		this.modules = new ArrayList<String>();
		
		for (Member channel : channels) {
			System.out.println(channel.getValue().asString());
			this.channels.add(new Channel(channel.getName(), channel.getValue().asString()));
		}
		
		for (JsonValue module : modules) {
			this.modules.add(module.asString());
		}
	}

	public JsonObject getJsonFile() {
		return this.jsonFile;
	}

	public String getFileName() {
		return this.fileName;
	}

	public String getServer() {
		return this.server;
	}

	public int getPort() {
		return this.port;
	}
	
	public String getNickname() {
		return this.nickname;
	}

	public String getDescription() {
		return this.description;
	}

	public String getPrefix() {
		return this.prefix;
	}

	public ArrayList<Channel> getChannels() {
		return this.channels;
	}

	public ArrayList<String> getModules() {
		return this.modules;
	}
	
}
