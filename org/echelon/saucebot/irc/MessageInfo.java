package org.echelon.saucebot.irc;

// Essentially a C/C++ struct, but that doesn't exist in Java, so I use a class instead.

public class MessageInfo {
	public Channel channel;
	
	public User user;
	public User kicker;
	public String newName;
	public String mode;
	
	public String message;
	public String[] arguments;
}
