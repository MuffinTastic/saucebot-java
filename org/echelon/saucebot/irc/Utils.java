package org.echelon.saucebot.irc;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Utils {
	private Utils() {}
	
	public static String joinStringArray(String[] strings) {
		return Utils.joinStringArray(strings, "");
	}
	
	public static String joinStringArray(String[] strings, String delimiter) {
		StringBuilder builder = new StringBuilder();
		
		for (int i = 0; i < strings.length; i++) {
			if (i < strings.length - 1 && delimiter.length() > 0)
				builder.append(strings[i] + delimiter);
			else
				builder.append(strings[i]);
		}
		
		return builder.toString();
	}
	
	public static String readFile(String path) throws IOException {
		return Utils.readFile(path, Charset.defaultCharset());
	}
	
	public static String readFile(String path, Charset encoding) throws IOException {
		  byte[] encoded = Files.readAllBytes(Paths.get(path));
		  return new String(encoded, encoding);
	}
	
}
