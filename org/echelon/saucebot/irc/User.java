package org.echelon.saucebot.irc;

import java.util.ArrayList;

public class User {
	private String userName;
	private String userHost;
	//private ArrayList<Channel> channels; // Can't implement! (Don't know how :[)
	
	public User(String userName, String userHost) {
		this.userName = userName;
		this.userHost = userHost;
	}

	public String getName() {
		return userName;
	}

	public void setName(String userName) {
		this.userName = userName;
	}
	
	public String getHost() {
		return this.userHost;
	}

	public void setHost(String userHost) {
		this.userHost = userHost;
	}
	
	public static User fromUserHost(String[] userHost) {
		if (userHost.length < 2)
			new Exception("User-Host String array must be 2 in length!").printStackTrace();
		
		return new User(userHost[0], userHost[1]);
	}
}
