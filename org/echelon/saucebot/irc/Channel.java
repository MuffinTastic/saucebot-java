package org.echelon.saucebot.irc;

import java.util.ArrayList;

public class Channel {
	private String channelName;
	private String channelPass;
	private String topic;
	private ArrayList<User> users;
	private ArrayList<User> opped;
	private ArrayList<User> voiced;
	
	public Channel(String channelName, String channelPass) {
		this.channelName = channelName;
		this.channelPass = channelPass;
		this.users = new ArrayList<User>();
		this.opped = new ArrayList<User>();
		this.voiced = new ArrayList<User>();
	}

	public String getName() {
		return this.channelName;
	}

	public void setName(String channelName) {
		this.channelName = channelName;
	}

	public String getPass() {
		return this.channelPass;
	}

	public void setPass(String channelPass) {
		this.channelPass = channelPass;
	}

	public String getTopic() {
		return this.topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public ArrayList<User> getUsers() {
		return this.users;
	}

	public void setUsers(ArrayList<User> users) {
		this.users = users;
	}
	
	public void addUser(User user) {
		this.users.add(user);
	}
	
	public boolean removeUser(User user) {
		return this.users.remove(user);
	}
	
	public boolean inChannel(User user) {
		return (this.users.contains(user));
	}

	public ArrayList<User> getOpped() {
		return opped;
	}

	public void setOpped(ArrayList<User> opped) {
		this.opped = opped;
	}
	
	public void addOp(User user) {
		this.opped.add(user);
	}
	
	public boolean removeOp(User user) {
		return this.opped.remove(user);
	}
	
	public boolean isOpped(User user) {
		return (this.opped.contains(user));
	}

	public ArrayList<User> getVoiced() {
		return voiced;
	}

	public void setVoiced(ArrayList<User> voiced) {
		this.voiced = voiced;
	}
	
	public void addVoiced(User user) {
		this.voiced.add(user);
	}
	
	public boolean removeVoiced(User user) {
		return this.voiced.remove(user);
	}
	
	public boolean isVoiced(User user) {
		return (this.voiced.contains(user));
	}
	
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(this.getClass().getName()); builder.append(" ");
		builder.append(this.getName()); builder.append(": ");
		builder.append(this.users.size()); builder.append(" users, topic: "); builder.append(this.getTopic());
		builder.append("\nUsers: \n");
		for (User user : this.users) {
			builder.append("	"); builder.append(user.getName()); builder.append("	"); builder.append((user.getHost() != null) ? user.getHost() : "(No IP tracked)");
			if (this.isOpped(user))
				builder.append(" (OPPED)");
			else if (this.isVoiced(user))
				builder.append(" (VOICED)");
			builder.append("\n");
		}
		return builder.toString();
	}
}
