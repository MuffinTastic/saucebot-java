package org.echelon.saucebot.irc;

import org.echelon.saucebot.Config;
import org.echelon.saucebot.Saucebot;
import org.echelon.saucebot.modules.ModuleSystem;
import org.echelon.saucebot.modules.ModuleSystem.EventType;

import java.util.HashMap;
import java.util.Map;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;

public class IRC {
	
	public enum ConnectionType {
		DISCONNECTED, CONNECTED, NAME_TAKEN, PING_TIMEOUT
	}
	
	private String server;
	private int port;
	
	private Config config;
	private ModuleSystem moduleSystem;
	
	private ConnectionType connectionStatus;
	
	private Socket socket;
	private BufferedReader socketIn;
	private BufferedWriter socketOut;
	
	private ArrayList<Channel> channels;
	private ArrayList<User> users;
	
	private HashMap<String, ArrayList<String>> messageCache;
	private Thread messageCacheThread;
	
	// Setup
	
	public IRC(String server, int port, Config config) {
		
		this.server = server;
		this.port = port;
		
		this.config = config;
		this.moduleSystem = Saucebot.getBot().getModuleSystem();
		
		this.connectionStatus = ConnectionType.DISCONNECTED;
		
		try {
			System.out.println("[BOT] Attempting to connect to " + server + "...");
			this.socket = new Socket(server, port);
			this.socketIn = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
			this.socketOut = new BufferedWriter(new OutputStreamWriter(this.socket.getOutputStream()));
		
			this.channels = new ArrayList<Channel>();
			this.users = new ArrayList<User>();
			ArrayList<Channel> configChans = this.config.getChannels();
			for (int i = 0; i < configChans.size(); i++) {
				this.channels.add(configChans.get(i));
			}
			
			this.messageCache = new HashMap<String, ArrayList<String>>();
			this.messageCacheThread = new MessageCacheThread(this.messageCache, this);
			
			setupUser();
		} catch (UnknownHostException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	public void setupUser() throws IOException {
		System.out.println("[BOT] Connected! Sending user info...");
		this.write("NICK " + this.config.getNickname());
		this.write("USER " + this.config.getNickname() + " 8 * :" + this.config.getDescription());
		System.out.println("[BOT] User info sent!");
		
		String line = null;
		while ((line = this.socketIn.readLine()) != null) {
			System.out.println("[IRC] " + line);
			if (line.indexOf("004") >= 0) { // logged in
				
				System.out.println("[BOT] Logged in! Joining channels...");
				
				for (int i = 0; i < this.channels.size(); i++) {
					this.join(this.channels.get(i));
				}
				
				this.connectionStatus = ConnectionType.CONNECTED;
				
				try{
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				this.messageCacheThread.start();
				
				break;
			
			} else if (line.indexOf("443") >= 0) {
				
				this.connectionStatus = ConnectionType.NAME_TAKEN;
				this.cleanUp();
				break;
				
			} else if (line.startsWith("PING "))
				this.write("PONG " + line.substring(5));
			
		}
		
		this.moduleSystem.callEvent(null, EventType.STARTUP);
	}
	
	// Raw functionality
	
	public void parse(String line) {
		MessageInfo info = new MessageInfo();
		
		System.out.println("[IRC] " + line);
		
		if (line.startsWith("PING "))
			this.write("PONG " + line.substring(5));
		else {
			String[] split = line.split(" ");
			
			String event = split[1];
			String channel = (split.length > 2) ? split[2] : null;
			String[] userhost = split[0].substring(1).split("\\!");
			
			if (event.equals("353") && channel.equals(this.config.getNickname())) { // list of users
				
				String[] chanUsers = Utils.joinStringArray(Arrays.copyOfRange(split, 4, split.length), " ").split(" \\:"); // ouch! good thing we don't do this often...
				Channel _channel = this.getChannelByName(chanUsers[0]);
				for (String nick : chanUsers[1].split(" ")) {
					
					String real_nick = (("+@").indexOf(nick.charAt(0)) >= 0) ? nick.substring(1) : nick;
					
					User tempUser = this.getUserByName(real_nick);
					if (tempUser == null) {
						tempUser = new User(real_nick, null);
						this.users.add(tempUser);
					}

					if (!_channel.inChannel(tempUser))
						_channel.addUser(tempUser);
					if (nick.charAt(0) == '@') _channel.addOp(tempUser);
					else if (nick.charAt(0) == '+') _channel.addVoiced(tempUser);
						
				}
			} else if (event.equals("332") && channel.equals(this.config.getNickname())) { // topic
				String[] chanTopic = Utils.joinStringArray(Arrays.copyOfRange(split, 3, split.length), " ").split(" \\:"); // ouch! good thing we don't do this often...
				Channel _channel = this.getChannelByName(chanTopic[0]);
				_channel.setTopic(chanTopic[1]);
			} else if (event.equals("PRIVMSG")) {
				User tempUser = this.getUserByName(userhost[0]);
				if (tempUser.getHost() == null)
					tempUser.setHost(userhost[1]);
				
				String message = Utils.joinStringArray(Arrays.copyOfRange(split, 3, split.length), " ").substring(1);
				info.user = tempUser;
				info.channel = this.getChannelByName(channel);
				info.message = message;
				
				if (message.startsWith(this.config.getPrefix())) {
					String[] arguments = message.split(" ");
					
					info.arguments = arguments;
					this.moduleSystem.callEvent(info, EventType.COMMAND);
				} else
					this.moduleSystem.callEvent(info, EventType.MESSAGE);
				
			} else if (event.equals("JOIN")) {
				
				
				User tempUser = this.getUserByName(userhost[0]);
				if (tempUser == null) {
					tempUser = new User(userhost[0], userhost[1]);
					this.users.add(tempUser);
				} else if (tempUser.getHost() == null)
					tempUser.setHost(userhost[1]);
				
				Channel tempChan = this.getChannelByName(channel);
				
				if (!tempChan.inChannel(tempUser)) {
					tempChan.addUser(tempUser);
					
					info.user = tempUser;
					info.channel = tempChan;
					
					this.moduleSystem.callEvent(info, EventType.JOIN);
				}
				
			} else if (event.equals("PART")) {
				User tempUser = this.getUserByName(userhost[0]);
				if (tempUser == null) {
					tempUser = new User(userhost[0], userhost[1]);
					this.users.add(tempUser);
				} else if (tempUser.getHost() == null)
					tempUser.setHost(userhost[1]);
				
				Channel tempChan = this.getChannelByName(channel);
				
				info.user = tempUser;
				info.channel = tempChan;
				info.message = Utils.joinStringArray(Arrays.copyOfRange(split, 3, split.length), " ").substring(1);
				
				tempChan.removeUser(tempUser);
				tempChan.removeOp(tempUser);
				tempChan.removeVoiced(tempUser);
				
				if (tempUser.getName() == this.config.getNickname())
					this.channels.remove(tempChan);
				
				this.moduleSystem.callEvent(info, EventType.LEAVE);
				
			} else if (event.equals("KICK")) {
				User tempKicker = this.getUserByName(userhost[0]);
				User tempUser = this.getUserByName(split[3]);
				if (tempKicker == null) {
					tempKicker = new User(userhost[0], userhost[1]);
					this.users.add(tempUser);
				} else if (tempKicker.getHost() == null)
					tempKicker.setHost(userhost[1]);
				if (tempUser == null) {
					tempUser = new User(userhost[0], null);
					this.users.add(tempUser);
				}
				
				Channel tempChan = this.getChannelByName(channel);
				
				info.kicker = tempKicker;
				info.user = tempUser;
				info.channel = tempChan;
				info.message = Utils.joinStringArray(Arrays.copyOfRange(split, 4, split.length), " ").substring(1);

				tempChan.removeUser(tempUser);
				tempChan.removeOp(tempUser);
				tempChan.removeVoiced(tempUser);
				
				if (tempUser.getName() == this.config.getNickname())
					this.channels.remove(tempChan);
				
				this.moduleSystem.callEvent(info, EventType.KICK);
				
			} else if (event.equals("QUIT")) {
				User tempUser = this.getUserByName(userhost[0]);
				if (tempUser == null) {
					tempUser = new User(userhost[0], userhost[1]);
					this.users.add(tempUser);
				} else if (tempUser.getHost() == null)
					tempUser.setHost(userhost[1]);
				
				Channel tempChan = this.getChannelByName(channel);
				
				info.user = tempUser;
				info.channel = tempChan;
				info.message = Utils.joinStringArray(Arrays.copyOfRange(split, 2, split.length), " ").substring(1);

				tempChan.removeUser(tempUser);
				tempChan.removeOp(tempUser);
				tempChan.removeVoiced(tempUser);
				this.users.remove(tempUser);
				
				this.moduleSystem.callEvent(info, EventType.QUIT);
				
			} else if (event.equals("NICK")) {
				User tempUser = this.getUserByName(userhost[0]);
				if (tempUser == null) {
					tempUser = new User(userhost[0], userhost[1]);
					this.users.add(tempUser);
				} else if (tempUser.getHost() == null)
					tempUser.setHost(userhost[1]);
				String newNick = Utils.joinStringArray(Arrays.copyOfRange(split, 2, split.length), " ").substring(1);
				
				tempUser.setName(newNick);
				
				info.user = tempUser;
				info.newName = newNick;

				this.moduleSystem.callEvent(info, EventType.NICK);
				
			} else if (event.equals("MODE")) {
				if (userhost[0].equals(split[2]))
					return;
				
				User tempSetter = this.getUserByName(userhost[0]);
				User tempUser = this.getUserByName(split[2]);
				if (tempSetter == null) {
					tempSetter = new User(userhost[0], userhost[1]);
					this.users.add(tempSetter);
				} else if (tempSetter.getHost() == null)
					tempSetter.setHost(userhost[1]);
				if (tempUser == null) {
					tempUser = new User(userhost[0], null);
					this.users.add(tempUser);
				}
				
				String mode = split[3];
				
				Channel tempChan = this.getChannelByName(channel);
				
				boolean mode_set = mode.startsWith("+");
				if (mode.endsWith("o")) {
					if (mode_set) {
						tempChan.addOp(tempUser);
					} else {
						tempChan.removeOp(tempUser);
					}
				} else if (mode.endsWith("v")) {
					if (mode_set) {
						tempChan.addVoiced(tempUser);
					} else {
						tempChan.removeVoiced(tempUser);
					}
				}
				
				info.user = tempUser;
				info.channel = tempChan;
				info.mode = mode;
				
				this.moduleSystem.callEvent(info, EventType.MODE);
			}
		}
	}
	
	public void write(String line) {
		try {
			this.socketOut.write(line + "\r\n");
			this.socketOut.flush();
		} catch (IOException e) {
			this.connectionStatus = ConnectionType.DISCONNECTED;
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	public String update() {
		String returnVal = null;
		try {
			if (this.messageCacheThread.isAlive())
				returnVal = this.socketIn.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return returnVal;
	}
	
	public void cleanUp() {
		try {
			this.connectionStatus = IRC.ConnectionType.DISCONNECTED;
			Thread.sleep(50);
			
			this.moduleSystem.callEvent(null, EventType.SHUTDOWN);
			
		} catch (InterruptedException f) {
			
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	public Channel getChannelByName(String name) {
		Channel retChan = null;
		
		for (int i = 0; i < this.channels.size(); i++) {
			if (this.channels.get(i).getName().equals(name)) {
				retChan = this.channels.get(i);
			}
		}
		
		return retChan;
	}
	
	public User getUserByName(String name) {
		User retUser = null;
		
		for (int i = 0; i < this.users.size(); i++) {
			if (this.users.get(i).getName().equals(name)) {
				retUser = this.users.get(i);
			}
		}
		
		return retUser;
	}
	
	// Actions
	
	public void send(Channel channel, String message) {
		if (channel == null)
			return;
		
		if (!this.messageCache.containsKey(channel.getName())) {
			this.messageCache.put(channel.getName(), new ArrayList<String>());
		}
		this.messageCache.get(channel.getName()).add(message);
	}
	
	public void join(Channel channel) {
		this.write("JOIN " + channel.getName() + " " + channel.getPass());
		if (!this.channels.contains(channel))
			this.channels.add(channel);
	}
	
	public void quit(String message) {
		this.connectionStatus = ConnectionType.DISCONNECTED;
		this.write("QUIT :" + message);
	}
	
	// Getters

	public String getServer() {
		return this.server;
	}

	public int getPort() {
		return this.port;
	}

	public ConnectionType getConnectionStatus() {
		return this.connectionStatus;
	}

	public Socket getSocket() {
		return this.socket;
	}

	public BufferedReader getSocketIn() {
		return this.socketIn;
	}

	public BufferedWriter getSocketOut() {
		return this.socketOut;
	}

	public Map<String, ArrayList<String>> getMessageCache() {
		return this.messageCache;
	}

	
}
