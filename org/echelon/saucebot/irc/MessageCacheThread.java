package org.echelon.saucebot.irc;

import org.echelon.saucebot.Saucebot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MessageCacheThread extends Thread {
	
	private HashMap<String, ArrayList<String>> messageCache;
	private IRC ircInstance;
	
	public MessageCacheThread(HashMap<String, ArrayList<String>> messageCache, IRC ircInstance) {
		this.messageCache = messageCache;
		this.ircInstance = ircInstance;
	}
	
	public void run() {
		try {
			while (this.ircInstance.getConnectionStatus() == IRC.ConnectionType.CONNECTED) {
				for (Map.Entry<String, ArrayList<String>> entry : this.messageCache.entrySet()) {
					if (entry.getValue().size() > 0) {
						boolean delayMessages = entry.getValue().size() > 3;
						for (int i = 0; i < entry.getValue().size(); i++) {
							if (delayMessages)
								this.sleep(1500);
						
							this.ircInstance.write("PRIVMSG " +
												entry.getKey() +
												" :" +
												entry.getValue().get(i));
						}

						for (int i = entry.getValue().size() - 1; i >= 0; i--) {
							entry.getValue().remove(i);
						}
					}
				}
			}
		} catch (InterruptedException f) {
			
		} catch (Exception e) {
			ircInstance.quit("Error occured!");
			e.printStackTrace();
			System.exit(1);
		}
	}
}